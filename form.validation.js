$(function() {
	$("form[name='contact']").validate({
		//Validation rules
		rules: {
			//The key name is the name attribute of an input field.
			//Define the validation rules for each field
            fullname: "required",
            phonenumber: "required",
			//If multiple rulls are required place in brackets
			emailaddress: {
				required: true,
				//use the build in email rule
				email: true
			},
            subject: "required",
            message: "required",
			
		},
		
		//Setup error messages
		messages: {
			fullname: "* Please enter your Full Name",
            phonenumber:  "* Please enter your Phone Number",
            subject: "* Please enter a subject",
            emailaddress: "* Please enter a valid email address",
            message: "* Please enter your message"
		},
		submitHandler: function(form) {
			form.submit();
		}
	});
});

$(function() {
	$("form[name='login']").validate({
		//Validation rules
		rules: {
			email: {
				required: true,
				//use the build in email rule
				email: true
			},
            password: {
                required: true
            }
			
		},
		
		//Setup error messages
		messages: {
            email: "Please enter a valid email address",
            password: "Please enter a password"
		},
		submitHandler: function(form) {
			form.submit();
		}
	});
});