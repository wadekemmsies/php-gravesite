<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>WEBM 1001 Final Project</title>
    <link rel="stylesheet" href="css/main.css" type="text/css">
    <link href="css/styles.css" rel="stylesheet" type="text/css">
    <link href="css/normalize.css" rel="stylesheet" type="text/css">
    <link href="css/w3.css" rel="stylesheet" type="text/css">
    <script
  src="https://code.jquery.com/jquery-3.4.1.min.js"
  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
  crossorigin="anonymous"></script>
    <script src="form.validation.js"></script>

    <script src="jquery.validate.js"></script>

    
  <script>
    $(document).ready(function(){
        $("input, textarea").focus(function(){
  // on focus
  $(this).css("border", "2px solid black");
});
$('input, textarea').blur(
    function(){
        $(this).css({"border" : "none", "border-bottom" : "1px solid #ddd"});
        
    });
    // $('#fullname').blur(function(){
    //     if( !$(this).val() ) {
    //         $("#fullNameErrorMessage").text("* Please enter your Full Name").addClass("error");
    //     } else {
    //         $("#fullNameErrorMessage").text("");
    //     }
    // });
    // $('#subject').blur(function(){
    //     if( !$(this).val() ) {
    //         $("#subjectErrorMessage").text("* Please enter a Subject").addClass("error");
    //     } else {
    //         $("#subjectErrorMessage").text("");
    //     }
    // });
    // $('#email').blur(function(){
    //     if( !$(this).val() ) {
    //         $("#emailErrorMessage").text("* Please enter your Email").addClass("error");
    //     } else {
    //         $("#emailErrorMessage").text("");
    //     }
    // });
    // $('#phonenumber').blur(function(){
    //     if( !$(this).val() ) {
    //         $("#phoneNumberErrorMessage").text("* Please enter your Phone Number").addClass("error");
    //     } else {
    //         $("#phoneNumberErrorMessage").text("");
    //     }
    // });
    // $('#message').blur(function(){
    //     if( !$(this).val() ) {
    //         $("#messageErrorMessage").text("* Please Enter a Message").addClass("error");
    //     } else {
    //         $("#messageErrorMessage").text("");
    //     }
    // });
    

    });

   
  </script>
</head>
<style>
    h3 {
        text-align: center;
    }

    .error {
        color: red;
    }

    .formclick {
        border-style: solid;
        border-width: 5px; 
    }
    
</style>
<body>
        
        <header>
                <div class="navigation">
                <a href="index.php">Home</a>
                <a href="about.php">About</a>
                <a href="faq.php">FAQ</a>
                <a href="contact.php">Contact</a>
                <a href="blog.php">Blog</a>
                        <a href="#openModal">Login</a>
                        <div id="openModal" class="modalDialog">
                            <div><a href="#close" title="Close" class="close">X</a>
                               
                                <div class="login">
                                    <h2>Sign in</h2>
                                    <form name="login">
                                        <input type="email" name="email" placeholder="Email">
                                        <input type="password" name="password" placeholder="Password">
                                        <input type="button" value="Login">
                                    </form>
                                </div>                 
                            </div>
                        </div>
                </div>
            </header>
                <div class="headerimg">
                    <img src="img/contact.jpg" width="100%" height="300px">
                </div>
                <?php
			
            //Check for header injection
            
            function has_header_injection($str){
                return preg_match( "/[\r\n]/", $str);
            }
            
            if (isset ($_POST['contact_submit'])){
                
                $name = trim($_POST['fullname']);
                $phonenumber = trim($_POST['phonenumber']);
                $subject = trim($_POST['subject']);
                $email = trim($_POST['emailaddress']);
                $msg = $_POST['message'];
                
                //Check to see if $name or $email have header incjections
                
            if (has_header_injection($name) || has_header_injection($email)) {
                die();  //if true, kill the script
                }
                
                if (!$name || !$email || !$msg || !$subject || !$phonenumber){
                    echo '<h4 class="error">All field required.</h4><a href="contact.php" class="button block">Go back and try again</a>';
                    exit;
                    
                }
                
                //add the recipient email to a variable. put YOUR email here!
                $to = "wkemmsies21@ymail.com";
                
                // Create a subject
                $subject = "$name sent you a message via your contact form";
                
                //construct the message
                $message = "Name: $name\r\n";
                $message .= "email: $email\r\n";
                $message .= "subject: $subject\r\n";
                $message .= "phone number: $phonenumber\r\n";
                $message .= "Message:\r\n$msg";
                
                //If the subscibe checkbox was checked
                // if (isset($_POST['subscribe']) && $_POST['subscribe'] == 'Subscribe'){
                    
                //     //Add a new line to the $message
                //     $message .= "\r\n\r\nPlease add $email to the mailing list.\r\n";
                    
                // }
                
                $message = wordwrap($message,72);
                
                //Set the mail headers into a variable
                
                $headers = "MIME-Version: 1.0\r\n";
                $headers .= "Content-type: text/plain; charset=iso-8859-1\r\n";
                $headers .= "From: $name <$email> \r\n";
                $headers .= "X-Priority: 1\r\n";
                $headers .= "X-MSMail-Priority: High\r\n\r\n";
                
                //Send the email
                mail($to, $subject, $message, $headers);
                
                
        ?>
        <h5>Thanks for contacting us!</h5>
		<p>Please allow 24 hours for a response.</p>
        <?php
            } else {
        ?>
            <article class="floatclear">
                <h1>Contact Us</h1>
                <hr>
                <h3>Send us a message and we will get back to you as soon as we can</h3>
                <form name="contact" action="" method="post" id="contact-form">
                    <div class="formcol-right">
                    <p id="fullNameErrorMessage"> <br></p>
                    <input id="fullname" type="text" name="fullname" placeholder="Full Name"> 
                    <p id="phoneNumberErrorMessage"> <br></p>
                    <input id="phonenumber" type="text" name="phonenumber" placeholder="Phone Number"> 
                </div>
                <div class="formcol-right">
                <p id="subjectErrorMessage"> <br></p> 
                    <input id="subject" type="text" name="subject" placeholder="Subject">
                    <p id="emailErrorMessage"> <br></p> 
                    <input id="email" type="text" name="emailaddress" placeholder="Email"></div>
             
                    <textarea class="textareasize" placeholder="Message" name="message" id="message"></textarea>
                    <p style="margin-left:45%" id="messageErrorMessage"> <br></p>
                    <input style="margin-left:50%" name="contact_submit" type="submit" value="Submit">
 
                </form>
            </article>
            <?php } ?>
            <footer>
                <nav>
                    <ul>
                        <li><a href="index.html">Home</a></li>
                        <li><a href="about.html">about</a></li>
                        <li><a href="faq.html">FAQ</a></li>
                        <li><a href="contact.html">Contact</a></li>
                        <li><a href="blog.html">Blog</a></li>
                    </ul>			   
                </nav>
            </footer>
            </body>
            </html>