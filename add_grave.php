<?php
require("connect.php");


if(isset($_POST['submit'])){
    $firstName = filter_input(INPUT_POST, 'firstName');    
    $middleName = filter_input(INPUT_POST, 'middleName');
    $lastName = filter_input(INPUT_POST, 'lastName');
    $birthDate = filter_input(INPUT_POST, 'birthDate');
    $deathDate = filter_input(INPUT_POST, 'deathDate');

    if($_POST['upload_key'] === 'graves123'){
        echo "Entered data successfully\n";
        header("Location:index.php");
    } else {
        $error = "Invalid Upload Key. Please try again";
        include("error.php");
        die();
        header("Location:add_grave.php");

        
    }
    // var_dump(basename($_FILES["fileToUpload"]["name"])); exit;
    // $PhotoName = filter_input(INPUT_POST, 'fileToUpload');
    $target_dir = "uploads/";
    $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
    $uploadOk = 1;
    $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
    // Check if image file is a actual image or fake image
    if(isset($_POST["submit"])) {
        $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
        if($check !== false) {
            // echo "File is an image - " . $check["mime"] . ".";
            header("Location:index.php");
            $uploadOk = 1;
        } else {
            echo "File is not an image.";
            $uploadOk = 0;
        }
    
    // Check if file already exists
    if (file_exists($target_file)) {
        echo "Sorry, file already exists.";
        $uploadOk = 0;
    }
    // Check file size
    if ($_FILES["fileToUpload"]["size"] > 500000) {
        echo "Sorry, your file is too large.";
        $uploadOk = 0;
    }
    // Allow certain file formats
    if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
    && $imageFileType != "gif" ) {
        echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
        $uploadOk = 0;
    }
    // Check if $uploadOk is set to 0 by an error
    if ($uploadOk == 0) {
        echo "Sorry, your file was not uploaded.";
    // if everything is ok, try to upload file
    } else {
        if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
            echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
        } else {
            echo "Sorry, there was an error uploading your file.";
        }
    }
}

$firstName = mysqli_real_escape_string($conn, $_POST['firstName']);
$lastName = mysqli_real_escape_string($conn, $_POST['lastName']);
$middleName = mysqli_real_escape_string($conn, $_POST['middleName']);

$birthDate = mysqli_real_escape_string($conn, $_POST['birthDate']);
$deathDate = mysqli_real_escape_string($conn, $_POST['deathDate']);
// $upload_key = mysqli_real_escape_string($conn, $_POST['upload_key']);


$sql = "INSERT INTO graves (firstName, middleName, lastName, birthDate, deathDate, PhotoName) 
VALUES ('$firstName', '$middleName' , '$lastName', '$birthDate', '$deathDate', '$target_file')";
// require("connect.php");
// // Escape user inputs for securit

$result = mysqli_query($conn, $sql);



if(!$result){    
    die("Could not enter data");
    include("error.php");
}

// // Escape user inputs for security


?>
 
<?php } else { ?>
    <!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="description" content="">
        <meta name="author" content="BATC">
        <title>The Grave Site</title>
        <link href='https://fonts.googleapis.com/css?family=Raleway' rel='stylesheet' type='text/css'>
        <link href="css/styles.css" rel="stylesheet" type="text/css">
        <link href="css/normalize.css" rel="stylesheet" type="text/css">
        <link href="css/w3.css" rel="stylesheet" type="text/css">
        <link href="css/main.css" rel="stylesheet" type="text/css">
        <script
  src="https://code.jquery.com/jquery-3.4.1.min.js"
  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
  crossorigin="anonymous"></script>
  <script>
    $(document).ready(function(){
        $("input, textarea").focus(function(){
  // on focus
  $(this).css("border", "2px solid black");
});
$('input, textarea').blur(
    function(){
        $(this).css({"border" : "none", "border-bottom" : "1px solid #ddd"});
    });
    });
  </script>

    </head>
    <body>
	    <header>
            <div class="navigation">
                <a href="index.php">Home</a>
                <a href="about.php">About</a>
                <a href="faq.php">FAQ</a>
                <a href="contact.php">Contact</a>
                <a href="blog.php">Blog</a>
                <a href="#openModal">Login</a>
                <div id="openModal" class="modalDialog">
                    <div><a href="#close" title="Close" class="close">X</a>
                       
                        <div class="login">
                            <h2>Sign in</h2>
                            <form>
                                <input type="email" name="email" placeholder="Email">
                                <input type="password" name="password" placeholder="Password">
                                <input type="button" value="Login">
                            </form>
                        </div>                 
                    </div>
                </div>
            </div> 
	    </header>
        <h1> Add Grave </h1>
        
        <body>
            <div class="addgrave">
        <form name="addgrave" method="post" action='<?php $_PHP_SELF ?>' enctype="multipart/form-data">
    
        <!-- <label for="firstName">First Name:</label> -->
        <input type="text" name="firstName" id="firstName" placeholder="First Name">
    
        <input type="text" name="middleName" id="middleName" placeholder="Middle Name">

        <!-- <label for="lastName">Last Name:</label> -->
        <input type="text" name="lastName" id="lastName" placeholder="Last Name">
    
        <!-- <label for="birthDate">Birth Date:</label> -->
        <input type="date" name="birthDate" id="birthDate" placeholder="Birth Date">
    
        <!-- <label for="deathDate">Death Date:</label> -->
        <input type="date" name="deathDate" id="birthDate" placeholder="Death Date">

        <input type="text" name="upload_key" id="upload_key" placeholder="Upload Key">

        Select image to upload:
        <input type="file" name="fileToUpload" id="fileToUpload">
        <input type="submit" name="submit" value="Submit">
        </form>
        </div>

            <?php }  ?>
        </body>