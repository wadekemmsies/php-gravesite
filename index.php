<!-- <?php 
require("connect.php");

$query = "SELECT * FROM graves";
$result = mysqli_query($conn, $query);
// $row = mysqli_fetch_array($result);
// // $image = $row['PhotoName'];

// // $image_src = "upload/".$image;
?> 


?> -->
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="description" content="">
        <meta name="author" content="BATC">
        <title>The Grave Site</title>
        <link href='https://fonts.googleapis.com/css?family=Raleway' rel='stylesheet' type='text/css'>
        <link href="css/styles.css" rel="stylesheet" type="text/css">
        <link href="css/normalize.css" rel="stylesheet" type="text/css">
        <link href="css/w3.css" rel="stylesheet" type="text/css">
        <link href="css/main.css" rel="stylesheet" type="text/css">
        <script
  src="https://code.jquery.com/jquery-3.4.1.min.js"
  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
  crossorigin="anonymous"></script>
  <script>
    $(document).ready(function(){
        $("input, textarea").focus(function(){
  // on focus
  $(this).css("border", "2px solid black");
});
    $('input, textarea').blur(
    function(){
        $(this).css({"border" : "none", "border-bottom" : "1px solid #ddd"});
    });
    $("img").mouseenter(function(){
        $(this).attr({"height" : "300", "width" : "325"})
        
    });
    $("img").mouseleave(function(){
        $(this).attr({"height" : "200", "width" : "225"})
    });
    
    
    });
  </script>

    </head>
    <body>
	    <header>
            <div class="navigation">
                <a href="index.php">Home</a>
                <a href="about.php">About</a>
                <a href="faq.php">FAQ</a>
                <a href="contact.php">Contact</a>
                <a href="blog.php">Blog</a>
                <a href="#openModal">Login</a>
                <div id="openModal" class="modalDialog">
                    <div><a href="#close" title="Close" class="close">X</a>
                       
                        <div class="login">
                            <h2>Sign in</h2>
                            <form name="login">
                                <input type="email" name="email" placeholder="Email">
                                <input type="password" name="password" placeholder="Password">
                                <input type="button" value="Login">
                            </form>
                        </div>                 
                    </div>
                </div>
            </div> 
	    </header>
	    
		<section id="hero">
	        <h1>Welcome to the Grave Site</h1> 
	       <div class="search">
		        <form action="index.html">
		    		<input type="search" name="ancestor_search" placeholder="Find your Ancestor">
		        </form>
            </div>   
	    </section>
        <section style="text-align: center">
        <?php foreach ($result as $row){ ?>
        <br>
            <img src='<?php echo $row['PhotoName'];  ?>'  height="200" width="225">
            <p><?php echo $row["firstName"] . " " . $row["middleName"] . " " . $row["lastName"]  . "<br> Birth Date: " . $row["birthDate"] . "<br> Death Date: " . $row["deathDate"];?></p>
        <?php } ?>          
        </div>
        <strong><a href="add_grave.php">Add Grave</a></strong>
            
        </section>
        <br>
		<footer>
			<nav>
				<ul>
					<li><a href="index.html">Home</a></li>
					<li><a href="about.html">about</a></li>
					<li><a href="faq.html">FAQ</a></li>
					<li><a href="contact.html">Contact</a></li>
					<li><a href="blog.html">Blog</a></li>
				</ul>			   
			</nav>
		</footer> 

    </body>
</html>