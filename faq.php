<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>WEBM 1001 Final Project</title>
    <link href="css/styles.css" rel="stylesheet" type="text/css">
    <link href="css/normalize.css" rel="stylesheet" type="text/css">
    <link href="css/w3.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="css/main.css">
    <script
  src="https://code.jquery.com/jquery-3.4.1.min.js"
  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
  crossorigin="anonymous"></script>
  <script>
    $(document).ready(function(){
        $("input, textarea").focus(function(){
  // on focus
  $(this).css("border", "2px solid black");
});
$('input, textarea').blur(
    function(){
        $(this).css({"border" : "none", "border-bottom" : "1px solid #ddd"});
    });
    });
  </script>
</head>
<body>
        <header>
            <div class="navigation">
            <a href="index.php">Home</a>
                <a href="about.php">About</a>
                <a href="faq.php">FAQ</a>
                <a href="contact.php">Contact</a>
                <a href="blog.php">Blog</a>
                    <a href="#openModal">Login</a>
                    <div id="openModal" class="modalDialog">
                            <div><a href="#close" title="Close" class="close">X</a>
                                
                                <div class="login">
                                    <h2>Sign in</h2>
                                    <form name="login">
                                        <input type="email" name="email" placeholder="Email">
                                        <input type="password" name="password" placeholder="Password">
                                        <input type="button" value="Login">
                                    </form>
                                </div>                 
                            </div>
                    </div>
            </div>
        </header>
                <div class="headerimg">
                    <img src="img/faq.jpg" width="100%" height="300x">
                </div>
            
            <article>
    <h2>Frequently Asked Questions</h2>
    <hr>
    <h3>How can I help?</h3>
    <p>
            Lorem Ipsum is simply dummy text of the printing and 
            typesetting industry. Lorem Ipsum has been the
             industry's standard dummy text ever since the 
             1500s, when an unknown printer took a galley 
             of type and scrambled it to make a type specimen
              book. It has survived not only five centuries,
               but also the leap into electronic typesetting,
                remaining essentially unchanged. It was 
                popularised in the 1960s with the release
                 of Letraset sheets containing Lorem Ipsum 
                 passages, and more recently with desktop 
                 publishing software like Aldus PageMaker 
                 including versions of Lorem Ipsum.
    </p>
    <h3>How can I get access?</h3>
    <p>
            Lorem Ipsum is simply dummy text of the printing and 
            typesetting industry. Lorem Ipsum has been the
             industry's standard dummy text ever since the 
             1500s, when an unknown printer took a galley 
             of type and scrambled it to make a type specimen
              book. It has survived not only five centuries,
               but also the leap into electronic typesetting,
                remaining essentially unchanged. It was 
                popularised in the 1960s with the release
                 of Letraset sheets containing Lorem Ipsum 
                 passages, and more recently with desktop 
                 publishing software like Aldus PageMaker 
                 including versions of Lorem Ipsum.
    </p>
    <h3>Best way to contact?</h3>
    <p>
            Lorem Ipsum is simply dummy text of the printing and 
            typesetting industry. Lorem Ipsum has been the
             industry's standard dummy text ever since the 
             1500s, when an unknown printer took a galley 
             of type and scrambled it to make a type specimen
              book. It has survived not only five centuries,
               but also the leap into electronic typesetting,
                remaining essentially unchanged. It was 
                popularised in the 1960s with the release
                 of Letraset sheets containing Lorem Ipsum 
                 passages, and more recently with desktop 
                 publishing software like Aldus PageMaker 
                 including versions of Lorem Ipsum.
    </p>
</article>
<footer>
			<nav>
				<ul>
					<li><a href="index.html">Home</a></li>
					<li><a href="about.html">about</a></li>
					<li><a href="faq.html">FAQ</a></li>
					<li><a href="contact.html">Contact</a></li>
					<li><a href="blog.html">Blog</a></li>
				</ul>			   
			</nav>
		</footer> 
</body>
</html>